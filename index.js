function getNumberOFImage() {

    var nInputBox = document.getElementById('inputBox').value
    var card = document.getElementById('card')
    var output = document.getElementById('output')

    while (card.hasChildNodes()) {
        card.removeChild(card.lastChild)
    }
    while (output.hasChildNodes()) {
        output.removeChild(output.lastChild)
    }

    for (let i = 1; i <= nInputBox; i++) {
        if (nInputBox > 5 || nInputBox == 0) {
            mess.innerHTML = "Enter number between 1 to 5";
        }
        else {

            // append Input
            var input = document.createElement('input')
            input.type = 'number'
            input.id = 'second' + i
            input.placeholder = `second for ${i} image`

            card.appendChild(input)

            // append button
            var btn = document.createElement('span')
            btn.innerHTML = `<button id="btn${i}" type="button" onClick="submitSecond(${i})">Submit</button><br>`
            card.appendChild(btn)

            // append Image
            var imgs = document.createElement('img');
            imgs.id = "image"+i
            output.appendChild(imgs);


        }
    }

}


var timeOut
var nTimeOut = {
    time1: 'time1',
    time2: 'time2',
    time3: 'time3',
    time4: 'time4',
    time5: 'time5',
}
function submitSecond(n) {

    if (nTimeOut[`time${n}`]) {
        clearInterval(nTimeOut[`time${n}`])
    }
    let nInputSecond = document.getElementById('second' + n).value

    let sFetchUrl = 'https://picsum.photos/200';

    nTimeOut[`time${n}`] = setInterval(() => {
        console.log(nInputSecond)
        fetch(sFetchUrl)
            .then(response => response.blob())
            .then(fetchBlob => {
                let imageObjectURL = URL.createObjectURL(fetchBlob)
                document.getElementById("image" + n).src = imageObjectURL
            })

    }, nInputSecond*1000)
}
